<?php require_once(ROOT_PATH."/templates/partials/header.php");?>
<main role="main" class="inner cover mt-5">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <?php foreach ($data['tree'] as $r): ?>
            <?php echo $r;?>
          <?php endforeach; ?>
        </div>
        <div class="col-sm-8">
          <div class="row">
            <?php foreach($data['products'] as $product):?>
            <div class="col-sm-6">
                <?php include(ROOT_PATH."/templates/partials/product_form.php"); ?>
            </div>
            <?php endforeach;?>
          </div>
        </div>
      </div>
    </div>
</main>
<script>
$(function() {
    $("ul li:not(:last-child) a").removeAttr('href');
});
</script>
<?php require_once(ROOT_PATH."/templates/partials/footer.php");?>