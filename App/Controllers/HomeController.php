<?php
namespace Shop\App\Controllers;

use \Shop\App\Controllers\Base\Controller;
use \Shop\App\Models\Product;
use \Shop\App\Models\Category;

class HomeController extends Controller
{

    public function indexAction($request = null)
    {
        $products = new Product();
        $products = $products->getHomeProducts();
        $categories = Category::getCategories();
        $tree = Category::fetchCategoryTreeList();
        $this->view->render(["products" => $products, "tree" => $tree]);
    }

    public function loginAction($request = null)
    {
        echo "login";
    }

    public function logoutAction($request = null)
    {
        
    }
}