<?php
namespace Shop\App\Controllers\Base;

class Controller
{

    protected $view;

    public function __construct($view)
    {
        $this->view = $view;
    }
}