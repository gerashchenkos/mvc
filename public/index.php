<?php
    define("ROOT_PATH", dirname(__FILE__, 2));
    define("SITE_URL", "http://mvc.loc");
    define("DB_USER", "root");
    define("DB_PASS", "root");
    define("DB_NAME", "lesson");
    session_start();

    require_once(ROOT_PATH."/vendor/autoload.php");
    
    use \Shop\Router;


    \Shop\App\Models\Base\DB::connect(DB_NAME, DB_USER, DB_PASS);
    $router = new Router();
    $router->setRoute("home", "index");
    $router->setRoute("home", "login");
    $router->run($_SERVER['REQUEST_URI']);
?>