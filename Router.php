<?php
namespace Shop;

class Router
{
    private $routes = [];

    public function setRoute($controller, $action)
    {
        if(!empty($this->routes[$controller])){
            if(!in_array($action, $this->routes[$controller])){
                $this->routes[$controller][] = $action;
            }
        } else{
            $this->routes[$controller][] = $action;
        }
    }

    public function run($query)
    {
        if($query !== "/"){
            $parts = explode("/", $query);
            $controller = $parts[1] ?? '';
            $action = $parts[2] ?? '';
        }

        if(empty($controller) && empty($action)){
            $controller = "home";
            $action = "index";
        }

        if(empty($action) || !isset($this->routes[$controller]) || !in_array($action, $this->routes[$controller])){
            header("HTTP/1.0 404 Not Found");
            return;
        }
        $view = new \Shop\App\Viewers\Viewer($controller, $action, ROOT_PATH);
        $controllerName = '\\Shop\\App\\Controllers\\'.ucfirst($controller)."Controller";
        $controllerObj = new $controllerName($view);
        $controllerObj->{$action."Action"}($_REQUEST);
    }
}